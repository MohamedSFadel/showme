using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Sinai.Business_Layer.Business;
using Sinai.Business_Layer.Interface;
using Sinai.Business_Layer.Repository;
using Sinai.Data_Access_Layer.Infrastructure;
using Sinai.Data_Access_Layer.Infrastructure.Contract;
using Sinai.Data_Access_Layer.Models;
using Sinai.Domain_Layer.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Sinai.Web_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //=================================================
            // For Entity Framework  
            services.AddDbContext<SinaiContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SinaiConnection")));

            // For Identity  
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequiredLength = 5;
                options.User.RequireUniqueEmail = true;

            }).AddEntityFrameworkStores<SinaiContext>()
                .AddDefaultTokenProviders();

            

            // Adding Authentication  
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })

            // Adding Jwt Bearer  
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration["JWT:ValidAudience"],
                    ValidIssuer = Configuration["JWT:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"])),
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                };
            });
            services.AddAuthorization(options =>
            {
                var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme);
                defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
                options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
            });
            //=================================================

            //=================================================

            // For Email
            var emailConfig = Configuration
                .GetSection("EmailConfiguration")
                .Get<EmailServices>();
            services.AddSingleton(emailConfig);

            //=================================================

            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            //------------------- User -------------------------------
            services.AddScoped<IUserBusiness, UserBusiness>();
            services.Configure<EmailServices>(Configuration.GetSection("MailSettings"));
            services.AddTransient<IEmailSenderBusiness, EmailSenderBusiness>();
            //--------------------------------------------------------

            //------------------- General -------------------------------
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            //-----------------------------------------------------------

            //------------------- Category -------------------------------
            services.AddTransient<ICategoryBusiness, CategoryBusiness>();
            services.AddTransient<ITestBusiness, TestBusiness>();
            //-------------------------------------------------------------


            services.AddControllers();
            services.AddMvc()
           .AddJsonOptions(options => {
               options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
               //options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault;
           });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Sinai.Web_API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sinai.Web_API v1")) ;
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

           
        }
    }
}
