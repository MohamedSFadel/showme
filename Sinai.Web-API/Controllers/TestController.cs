﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sinai.Business_Layer.Interface;
using Sinai.Domain_Layer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sinai.Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        ITestBusiness _testBusiness;
        public TestController(ITestBusiness testBusiness)
        {
            _testBusiness = testBusiness;
        }

        [HttpGet]
        public ActionResult Get()
        {
            List<CategoryDomainModel> categories = _testBusiness.getAllCat();

            return Ok(categories);
        }
        
    }
}
