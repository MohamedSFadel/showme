﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sinai.Business_Layer.Interface;
using Sinai.Domain_Layer.DomainModels;
using Sinai.Domain_Layer.Response;
using Sinai.Domain_Layer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sinai.Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private ICategoryBusiness _categoryBusiness;
        public CategoryController(ICategoryBusiness categoryBusiness)
        {
            _categoryBusiness = categoryBusiness;
        }

        [HttpGet("Categories")]
        public async Task<ActionResult> GetAllCategories()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryBusiness.GetCategories();

                    if (result.IsSuccess)
                        return Ok(result); // status code: 200

                    return BadRequest(result);
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch(Exception ex)
            {
                return BadRequest(new GeneralResponse<CategoryDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                });
            }
        }

        [HttpGet("Hotels")]
        public ActionResult GetAllHotels(int page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryBusiness.GetHotels(page);
                    if (result.IsSuccess)
                        return Ok(result);
                    return BadRequest(result);
                }
                return BadRequest("Some Error");
            }
            catch(Exception ex)
            {
                return BadRequest(new GeneralResponse<HotelViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                });
            }
        }

        [HttpGet("Chalets")]
        public ActionResult GetAllChalets(int page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryBusiness.GetChalets(page);
                    if (result.IsSuccess)
                        return Ok(result);
                    return BadRequest(result);
                }
                return BadRequest("Some Error");
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<ChaletDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                });
            }
        }

        [HttpGet("Cars")]
        public ActionResult GetAllCars(int page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryBusiness.GetCars(page);
                    if (result.IsSuccess)
                        return Ok(result);
                    return BadRequest(result);
                }
                return BadRequest("Some Error");
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<CarDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                });
            }
        }

        [HttpGet("Trips")]
        public ActionResult GetAllTrips(int page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryBusiness.GetTrips(page);
                    if (result.IsSuccess)
                        return Ok(result);
                    return BadRequest(result);
                }
                return BadRequest("Some Error");
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<TripDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                });
            }
        }

        [HttpGet("Tourisms")]
        public ActionResult GetAllTourisms(int page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _categoryBusiness.GetTourisms(page);
                    if (result.IsSuccess)
                        return Ok(result);
                    return BadRequest(result);
                }
                return BadRequest("Some Error");
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<TourismDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                });
            }
        }


    }
}
