﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Sinai.Business_Layer.Interface;
using Sinai.Domain_Layer.DomainModels;
using Sinai.Domain_Layer.Response;
using Sinai.Domain_Layer.ViewModel;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static Sinai.Business_Layer.Business.UserBusiness;

namespace Sinai.Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    { 
        private IUserBusiness _userBusiness;
        private IEmailSenderBusiness _emailSenderBusiness;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserBusiness userBusiness, IEmailSenderBusiness emailSenderBusiness, ILogger<UserController> logger)
        {
            _userBusiness = userBusiness;
            _emailSenderBusiness = emailSenderBusiness;
            _logger = logger;
        }

        [HttpPost("Register")]
        public async Task<ActionResult> RegisterAsync([FromForm] RegisterViewModel userModel )
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _userBusiness.RegisterUserAsync(userModel);

                    if (result.IsSuccess)
                        return Ok(result); // status code: 200

                    return BadRequest(result);
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }
        
        [HttpPost("Login")]
        public async Task<ActionResult> LoginAsync([FromBody] RegisterViewModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _userBusiness.LoginUserAsync(userModel);

                    if (result.IsSuccess)
                        return Ok(result); // status code: 200

                    return BadRequest(result);
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

        //[HttpGet("send")]
        //public async Task<IActionResult> SendMail()
        //{
        //    try
        //    {
        //        var userId = User.FindFirst(ClaimTypes.NameIdentifier);

        //        var message = new MessageServices(new string[] { "esraamohammad2018@gmail.com" }, "Test email", "This is the content from our email.");
        //        _emailSenderBusiness.SendEmail(message);

        //        //await _emailSenderBusiness.SendEmailAsync(request);
        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //}

        [HttpGet("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(token))
                return NotFound();
            var result = await _userBusiness.ConfirmEmailAsync(userId, token);

            if (result.IsSuccess)
                return Ok(result);

            return BadRequest(result);

        }

        [HttpPost("ForgetPassowrd")]
        public async Task<IActionResult> ForgetPassword([FromBody] RegisterViewModel userModel)
        {
            try
            {
                if (string.IsNullOrEmpty(userModel.user_email))
                    return NotFound();

                var result = await _userBusiness.ForgetPasswordAsync(userModel);

                if (result.IsSuccess)
                    return Ok(result);

                return BadRequest(result);
            }
            catch(Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
            

        }

        [HttpPost("RestPassowrd")]
        public async Task<IActionResult> RestPassword([FromBody] RegisterViewModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _userBusiness.RestPasswordAsync(userModel);

                    if (result.IsSuccess)
                        return Ok(result); // status code: 200

                    return BadRequest(result);
                }

                return BadRequest("Some properties are not valid");
            }
            catch(Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] RegisterViewModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var headers = Request.Headers;

                    if (headers.ContainsKey("token"))
                    {
                        string token = headers.GetCommaSeparatedValues("token").First();
                        var result = await _userBusiness.ChangePasswordAsync(userModel, token);

                        if (result.IsSuccess)
                            return Ok(result); // status code: 200

                        return BadRequest(result);
                    }
                }
                return BadRequest("Some properties are not valid");
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

        [HttpGet("LogOut")]
        public async Task<IActionResult> LogOut()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _userBusiness.LogOutUserAsync();
                    return Ok(result);
                }

                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ModelState.ToString(),
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {
                                   Key = ModelState.ToString(),
                                   Value = ModelState.ToString()
                               }
                            }
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }

        }

        [HttpPost("Verfication")]
        public async Task<ActionResult> VerficationAsync([FromBody] RegisterViewModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _userBusiness.ActiveCodeAsync(userModel.user_email, userModel.code);

                    if (result.IsSuccess)
                        return Ok(result); // status code: 200

                    return BadRequest(result);
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

        [HttpPost("ResetCode")]
        public async Task<ActionResult> ResetVerficationAsync([FromBody] RegisterViewModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _userBusiness.RestActiveCodeAsync(userModel.user_email);

                    if (result.IsSuccess)
                        return Ok(result); // status code: 200

                    return BadRequest(result);
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

        [HttpGet("Profile")]
        public async Task<IActionResult> ProfileUser()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var headers = Request.Headers;
                    
                    if (headers.ContainsKey("token"))
                    {
                        string token = headers.GetCommaSeparatedValues("token").First();
                        var result = await _userBusiness.GetUserAsync(token);

                        if (result.IsSuccess)
                            return Ok(result); // status code: 200

                        return BadRequest(result);
                    }
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

        [HttpPost("Profile")]
        public async Task<ActionResult> ProfileUser([FromForm] RegisterViewModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var headers = Request.Headers;

                    if (headers.ContainsKey("token"))
                    {
                        string token = headers.GetCommaSeparatedValues("token").First();
                        var result = await _userBusiness.UpdateUserAsync(userModel, token);

                        if (result.IsSuccess)
                            return Ok(result); // status code: 200

                        return BadRequest(result);
                    }
                }
                return BadRequest("Some properties are not valid"); // status code: 400
            }
            catch (Exception ex)
            {
                return BadRequest(new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                });
            }
        }

       

    }
}
