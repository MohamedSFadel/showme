﻿using Newtonsoft.Json;
using Sinai.Business_Layer.Interface;
using Sinai.Data_Access_Layer.Infrastructure.Contract;
using Sinai.Data_Access_Layer.Models;
using Sinai.Data_Access_Layer.Repository;
using Sinai.Domain_Layer.DomainModels;
using Sinai.Domain_Layer.Response;
using Sinai.Domain_Layer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Business
{
    public class CategoryBusiness : ICategoryBusiness
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly CategoryRepository categoryRepository;
        SinaiContext _sinaiContext;

        public int pageSize = 10;
        
        public CategoryBusiness(IUnitOfWork unitOfWork, SinaiContext sinaiContext)
        {
            _unitOfWork = unitOfWork;
            categoryRepository = new CategoryRepository(_unitOfWork);
            _sinaiContext = sinaiContext;
        }


        public GeneralResponse<CategoryDomainModel> GetCategories()
        {
            try
            {
                List<CategoryDomainModel> result = _sinaiContext.Categories.Where(cat => cat.Active == true).Select(cat => new CategoryDomainModel
                {
                    Id = cat.Id,
                    //CategoryNameAr = cat.CategoryNameAr,
                    CategoryNameEn = cat.CategoryNameEn
                }).ToList();


                if (result.Count == 0)
                    return new GeneralResponse<CategoryDomainModel>
                    {
                        Message = "No Categories Found",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {
                                    Key = "Categories",
                                    Value = "No Categories Found"
                                }
                            }
                        }
                    };

                return new GeneralResponse<CategoryDomainModel>
                {
                    Message = "Categories Successfully! ",
                    IsSuccess = true,
                    Result = new ResultResponse<CategoryDomainModel>
                    {
                        Categories = result
                    }
                };
            }
            catch(Exception ex)
            {
                return new GeneralResponse<CategoryDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                };
            }
        }

        public GeneralResponse<HotelViewModel> GetHotels(int  page)
        {
            try {
                var result = _sinaiContext.Hotels
                    .Join(
                        _sinaiContext.Locations,
                        hot => hot.LocationId,
                        loc => loc.Id,
                        (hot, loc) => new
                        {
                            hotel = hot,
                            location = loc
                        })
                    .Select(all => new HotelViewModel
                    {
                        id = all.hotel.Id,
                        title = all.hotel.Title,
                        details = all.hotel.Details,
                        Active = all.hotel.Active,
                        CategoryId = all.hotel.CategoryId,
                        //hotel_image = all.media.MediaUrl,
                        //hotel_image_type = all.media.MediaType,
                        Lat = all.location.Lat,
                        Lng = all.location.Lng,
                        Location_name = all.location.Title,
                       // CityId = all.location.CityId
                    })
                    .Where(hot => hot.Active == true && hot.CategoryId == 1 )
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                if (result.Count == 0)
                    return new GeneralResponse<HotelViewModel>
                    {
                        Message = "No Hotels Found",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {
                                    Key = "Hotels",
                                    Value = "No Hotels Found"
                                }
                            }
                        }
                    };

                //List<MediaDomainModel> resultImage = new List<MediaDomainModel>() ;
                MediaDomainModel resultImage = new MediaDomainModel() ;
                double resultRoomPrice = 0.0;
                List<HotelViewModel> Allresult = new List<HotelViewModel>() ;

                foreach (var item in result)
                {
                    resultImage = _sinaiContext.Media.Where(med => med.CategoryId == item.CategoryId && med.CategoryTypeId == item.id).Select(med => new MediaDomainModel
                    {
                        MediaUrl = med.MediaUrl,
                        MediaType = med.MediaType,
                    }).FirstOrDefault();

                    resultRoomPrice = _sinaiContext.Rooms.Where(room => room.HotelId == item.id).Min(room => room.RoomPrice).Value;

                    Allresult.Add(
                        new HotelViewModel()
                        {
                            id = item.id,
                            title = item.title,
                            //details = item.details,
                            start_price = resultRoomPrice,
                            rate = 3,
                            Lat = item.Lat,
                            Lng = item.Lng,
                            Location_name = item.Location_name,
                            //CityId = item.CityId,
                            image = resultImage.MediaUrl

                        });
                }

                return new GeneralResponse<HotelViewModel>
                {
                    Message = "Hotels Successfully! ",
                    IsSuccess = true,
                    Result = new ResultResponse<HotelViewModel>
                    {

                        Hotels = Allresult
                    }
                };
            }
            catch(Exception ex)
            {
                return new GeneralResponse<HotelViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                };
            }
        }

        

        public GeneralResponse<ChaletDomainModel> GetChalets(int page)
        {
            try
            {

                var result = _sinaiContext.Chalets
                .Join(
                    _sinaiContext.Locations,
                    chal => chal.LocationId,
                    loc => loc.Id,
                    (chal, loc) => new ChaletDomainModel
                    {
                        id = chal.Id,
                        title = chal.Title,
                        details = chal.Details,
                        phone = chal.Phone,
                        price = chal.ChaletPrice,
                        active = chal.Active,
                        category_id = chal.CategoryId,
                        location_lat = loc.Lat,
                        location_lng = loc.Lng,
                        location_name = loc.Title,
                        //city_id = loc.CityId
                    })
                .Where(chal => chal.active == true && chal.category_id == 2)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            if (result.Count == 0)
                return new GeneralResponse<ChaletDomainModel>
                {
                    Message = "No Apartment and chalets Found",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = "Apartment and chalets",
                                Value = "No Apartment and chalets Found"
                            }
                        }
                    }
                };
            //List<MediaDomainModel> resultImage = new List<MediaDomainModel>();
            MediaDomainModel resultImage = new MediaDomainModel();
            List<ChaletDomainModel> Allresult = new List<ChaletDomainModel>();
                //foreach (var item in result)
                //{
                //    resultImage = _sinaiContext.Media.Where(med => med.CategoryId == item.category_id && med.CategoryTypeId == item.id).Select(med => new MediaDomainModel
                //    {
                //        MediaUrl = med.MediaUrl,
                //        MediaType = med.MediaType,
                //    }).ToList();

                //    Allresult.Add(
                //        new ChaletDomainModel()
                //        {
                //            id = item.id,
                //            title = item.title,
                //        //details = item.details,
                //        //phone = item.phone,
                //        price = item.price,
                //        //room_count = item.room_count,
                //        location_lat = item.location_lat,
                //            location_lng = item.location_lng,
                //            location_name = item.location_name,
                //        //city_id = item.city_id,
                //        images = resultImage

                //        });
                //}

                foreach (var item in result)
                {
                    resultImage = _sinaiContext.Media.Where(med => med.CategoryId == item.category_id && med.CategoryTypeId == item.id).Select(med => new MediaDomainModel
                    {
                        MediaUrl = med.MediaUrl,
                        MediaType = med.MediaType,
                    }).FirstOrDefault();

                    Allresult.Add(
                        new ChaletDomainModel()
                        {
                            id = item.id,
                            title = item.title,
                            //details = item.details,
                            //phone = item.phone,
                            price = item.price,
                            rate = 3,
                            //room_count = item.room_count,
                            location_lat = item.location_lat,
                            location_lng = item.location_lng,
                            location_name = item.location_name,
                            //city_id = item.city_id,
                            image = resultImage.MediaUrl

                        });
                }


                return new GeneralResponse<ChaletDomainModel>
                {
                    Message = "Apartment and chalets Successfully! ",
                    IsSuccess = true,
                    Result = new ResultResponse<ChaletDomainModel>
                    {

                        Chalets = Allresult
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<ChaletDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
}
                    }
                };
            }
        }

        public GeneralResponse<CarDomainModel> GetCars(int page)
        {
            try
            {

                List<CarDomainModel> result = _sinaiContext.Cars.Where(car => car.Active == true && car.CategoryId == 3).Select(car => new CarDomainModel
                {
                    id = car.Id,
                    title = car.Title,
                    details = car.Details,
                    phone = car.Phone,
                    price = car.CarPrice,
                    driver = car.Driver,
                    category_id = car.CategoryId
                })
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

                if (result.Count == 0)
                    return new GeneralResponse<CarDomainModel>
                    {
                        Message = "No Cars Found",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = "Cars",
                                Value = "No Cars Found"
                            }
                        }
                        }
                    };

                //List<MediaDomainModel> resultImage = new List<MediaDomainModel>();
                MediaDomainModel resultImage = new MediaDomainModel();

                List<CarDomainModel> Allresult = new List<CarDomainModel>();
                foreach (var item in result)
                {
                    resultImage = _sinaiContext.Media.Where(med => med.CategoryId == item.category_id && med.CategoryTypeId == item.id).Select(med => new MediaDomainModel
                    {
                        MediaUrl = med.MediaUrl,
                        MediaType = med.MediaType,
                    }).FirstOrDefault();

                    Allresult.Add(
                        new CarDomainModel()
                        {
                            id = item.id,
                            title = item.title,
                            //details = item.details,
                            //phone = item.phone,
                            price = item.price,
                            rate = 3,
                            image = resultImage.MediaUrl

                        });
                }

                return new GeneralResponse<CarDomainModel>
                {
                    Message = "Cars Successfully! ",
                    IsSuccess = true,
                    Result = new ResultResponse<CarDomainModel>
                    {

                        Cars = Allresult
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<CarDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
}
                    }
                };
            }
        }

        public GeneralResponse<TripDomainModel> GetTrips(int page)
        {
            try
            {
                var result = _sinaiContext.Trips
                .Join(
                    _sinaiContext.Locations,
                    trip => trip.LocationId,
                    loc => loc.Id,
                    (trip, loc) => new TripDomainModel
                    {
                        id = trip.Id,
                        title = trip.Title,
                        details = trip.Details,
                        phone = trip.Phone,
                        price = trip.TripPrice,
                        start_day = trip.StartDay,
                        end_day = trip.EndDay,
                        days = trip.Days,
                        nights = trip.Night,
                        time = trip.Time,
                        active = trip.Active,
                        category_id = trip.CategoryId,
                        location_lat = loc.Lat,
                        location_lng = loc.Lng,
                        location_name = loc.Title,
                        //city_id = loc.CityId
                    })
                .Where(trip => trip.active == true && trip.category_id == 4)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

                if (result.Count == 0)
                    return new GeneralResponse<TripDomainModel>
                    {
                        Message = "No Trips Found",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {
                                    Key = "Trips",
                                    Value = "No Trips Found"
                                }
                            }
                        }
                    };

                //List<MediaDomainModel> resultImage = new List<MediaDomainModel>();
                MediaDomainModel resultImage = new MediaDomainModel();
                List<TripDomainModel> Allresult = new List<TripDomainModel>();
                foreach (var item in result)
                {
                    resultImage = _sinaiContext.Media.Where(med => med.CategoryId == item.category_id && med.CategoryTypeId == item.id).Select(med => new MediaDomainModel
                    {
                        MediaUrl = med.MediaUrl,
                        MediaType = med.MediaType,
                    }).FirstOrDefault();

                    Allresult.Add(
                        new TripDomainModel()
                        {
                            id = item.id,
                            title = item.title,
                            //details = item.details,
                            rate = 3,
                            location_lat = item.location_lat,
                            location_lng = item.location_lng,
                            location_name = item.location_name,
                            //city_id = item.city_id,
                            image = resultImage.MediaUrl

                        });
                }

                return new GeneralResponse<TripDomainModel>
                {
                    Message = "Trips Successfully! ",
                    IsSuccess = true,
                    Result = new ResultResponse<TripDomainModel>
                    {

                        Hotels = Allresult
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<TripDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                };
            }
        }
        public GeneralResponse<TourismDomainModel> GetTourisms(int page)
        {
            try
            {
                var result = _sinaiContext.Tourisms
                .Join(
                    _sinaiContext.Locations,
                    tour => tour.LocationId,
                    loc => loc.Id,
                    (tour, loc) => new TourismDomainModel
                    {
                        id = tour.Id,
                        title = tour.Title,
                        details = tour.Details,
                        active = tour.Active,
                        category_id = tour.CategoryId,
                        location_lat = loc.Lat,
                        location_lng = loc.Lng,
                        location_name = loc.Title,
                        //city_id = loc.CityId
                    })
                .Where(tour => tour.active == true && tour.category_id == 5)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

                if (result.Count == 0)
                    return new GeneralResponse<TourismDomainModel>
                    {
                        Message = "No Tourisms Found",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {
                                    Key = "Tourisms",
                                    Value = "No Tourisms Found"
                                }
                            }
                        }
                    };

                //List<MediaDomainModel> resultImage = new List<MediaDomainModel>();
                MediaDomainModel resultImage = new MediaDomainModel();
                List<TourismDomainModel> Allresult = new List<TourismDomainModel>();
                foreach (var item in result)
                {
                    resultImage = _sinaiContext.Media.Where(med => med.CategoryId == item.category_id && med.CategoryTypeId == item.id).Select(med => new MediaDomainModel
                    {
                        MediaUrl = med.MediaUrl,
                        MediaType = med.MediaType,
                    }).FirstOrDefault();

                    Allresult.Add(
                        new TourismDomainModel()
                        {
                            id = item.id,
                            title = item.title,
                            rate = 3,
                            //details = item.details,
                            location_lat = item.location_lat,
                            location_lng = item.location_lng,
                            location_name = item.location_name,
                            //city_id = item.city_id,
                            image = resultImage.MediaUrl

                        });
                }

                return new GeneralResponse<TourismDomainModel>
                {
                    Message = "Tourisms Successfully! ",
                    IsSuccess = true,
                    Result = new ResultResponse<TourismDomainModel>
                    {

                        Tourisms = Allresult
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<TourismDomainModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {
                            new DetailsResponse
                            {
                                Key = string.Join(",", ex.Data),
                                Value = string.Join(",", ex.InnerException)
                            }
                        }
                    }
                };
            }
        }


    }


}
