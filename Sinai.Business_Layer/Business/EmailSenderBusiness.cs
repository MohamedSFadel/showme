﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Sinai.Business_Layer.Interface;
using Sinai.Domain_Layer.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Business
{
    public class EmailSenderBusiness : IEmailSenderBusiness
    {
        private readonly EmailServices _emailServices;
        private readonly IConfiguration _configuration;
        public EmailSenderBusiness(EmailServices emailServices, IConfiguration configuration)
        {
            _emailServices = emailServices;
            _configuration = configuration;
        }

        public void SendEmail(MessageServices message)
        {
            var emailMessage = CreateEmailMessage(message);
            Send(emailMessage);
        }

        private MimeMessage CreateEmailMessage(MessageServices message)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_emailServices.From));
            emailMessage.To.AddRange(message.To);
            emailMessage.Subject = message.Subject;
            var bodyBuilder = new BodyBuilder { HtmlBody = string.Format("<h2 style='color:red;'>{0}</h2>", message.Content) };

            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text) { Text = message.Content };
            return emailMessage;
        }
        private void Send(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(_emailServices.SmtpServer, _emailServices.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_emailServices.UserName, _emailServices.Password);
                    client.Send(mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception or both.
                    throw;
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }

        //public async Task SendEmailAsync(MessageServices mailRequest)
        //{
        //    var email = new MimeMessage();
        //    email.Sender = MailboxAddress.Parse(_emailServices.From);
        //    email.To.Add(MailboxAddress.Parse(mailRequest.To));
        //    email.Subject = mailRequest.Subject;
        //    var builder = new BodyBuilder();
        //    //if (mailRequest.Attachments != null)
        //    //{
        //    //    byte[] fileBytes;
        //    //    foreach (var file in mailRequest.Attachments)
        //    //    {
        //    //        if (file.Length > 0)
        //    //        {
        //    //            using (var ms = new MemoryStream())
        //    //            {
        //    //                file.CopyTo(ms);
        //    //                fileBytes = ms.ToArray();
        //    //            }
        //    //            builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
        //    //        }
        //    //    }
        //    //}
        //    builder.HtmlBody = mailRequest.Content;
        //    email.Body = builder.ToMessageBody();
        //    using var smtp = new SmtpClient();
        //    smtp.Connect(_configuration["MailSettings:Host"], _emailServices.Port, SecureSocketOptions.StartTls);
        //    smtp.Authenticate(_emailServices.UserName, _emailServices.Password);
        //    await smtp.SendAsync(email);
        //    smtp.Disconnect(true);
        //}
    }
}
