﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Sinai.Business_Layer.Helper;
using Sinai.Business_Layer.Interface;
using Sinai.Data_Access_Layer.Models;
using Sinai.Domain_Layer.DomainModels;
using Sinai.Domain_Layer.Response;
using Sinai.Domain_Layer.ViewModel;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Business
{
    public class UserBusiness : IUserBusiness
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private IConfiguration _configuration;
        private IEmailSenderBusiness _emailSenderBusiness;

        private SinaiContext _context;

        public static IHostingEnvironment _environment;

        public UserBusiness(UserManager<ApplicationUser> userManager, IConfiguration configuration, IEmailSenderBusiness emailSenderBusiness, SignInManager<ApplicationUser> signInManager, SinaiContext context, IHostingEnvironment environment)
        {
            _userManager = userManager;
            _configuration = configuration;
            _emailSenderBusiness = emailSenderBusiness;
            _signInManager = signInManager;
            _context = context;
            _environment = environment;
        }

        public async Task<GeneralResponse<RegisterViewModel>> RegisterUserAsync(RegisterViewModel userModel)
        {
            try
            {
                //if (userModel == null)
                //    throw new NullReferenceException("Register Model is null");

                if (userModel.user_password != userModel.user_password_confirm)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Confirm password doesn't match the password",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {
                                   Key = "Password Or Confirm Password",
                                   Value = "Confirm password doesn't match the password"
                               }
                            }
                        }
                    };

                var locationUser = new Location()
                {
                    Lat = userModel.user_lat,
                    Lng = userModel.user_lng,
                    Title = userModel.user_location
                };

                var resultLoction = _context.Locations.Add(locationUser);
                _context.SaveChanges();

                ApplicationUser applicationUser;

                //----------------------app id --------------------------------
                bool user_app_found = true;
                //var user_app = RandomHelper.RandonString(6);

                //var users = _context.Users.Where(us => us.UserIdApp == user_app);

                string user_app;
                for (var i = 0; user_app_found == true; i++)
                {
                    user_app = RandomHelper.RandonString(15);

                    var users = _context.Users.Where(us => us.UserIdApp == user_app).Count();
                    if (users != 0)
                    {
                        user_app_found = true;
                    }
                    else if (users == 0)
                    {
                        user_app_found = false;
                       
                        if (userModel.user_image != null)
                        {
                            try
                            {
                                if (!Directory.Exists(_environment.WebRootPath + "\\uploads\\"))
                                {
                                    Directory.CreateDirectory(_environment.WebRootPath + "\\uploads\\");
                                }
                                string userImage = "\\uploads\\" + RandomHelper.RandonString(25) + Path.GetExtension(userModel.user_image.FileName).ToLower();
                                using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + userImage))
                                {
                                    userModel.user_image.CopyTo(filestream);
                                    filestream.Flush();
                                    var fileExtention = Path.GetExtension(userModel.user_image.FileName).ToLower();
                                    //userImage = "\\uploads\\" +  RandomHelper.RandonString(25) + fileExtention;
                                }

                                applicationUser = new ApplicationUser()
                                {
                                    Email = userModel.user_email,
                                    UserName = userModel.user_email,
                                    FullName = userModel.user_name,
                                    PasswordHash = userModel.user_password,
                                    LocationId = locationUser.Id,
                                    //LocationId = 8,
                                    PhoneNumber = userModel.user_phone,
                                    UserImg = userImage,
                                    SecurityCode = RandomHelper.RandonString(5),
                                    UserIdApp = user_app
                                };

                                
                            }
                            catch (Exception ex)
                            {
                                return new GeneralResponse<RegisterViewModel>
                                {
                                    Message = ex.Message,
                                    Error = new ErrorResponse
                                    {
                                        Details = new List<DetailsResponse>
                                        {

                                            new DetailsResponse
                                            {

                                                Key = string.Join(",", ex.Data),
                                                Value = string.Join(",", ex.InnerException)
                                            }
                                        }
                                    }
                                };
                            }
                        }
                        else
                        {
                            applicationUser = new ApplicationUser()
                            {
                                Email = userModel.user_email,
                                UserName = userModel.user_email,
                                FullName = userModel.user_name,
                                PasswordHash = userModel.user_password,
                                LocationId = locationUser.Id,
                                //LocationId = 8,
                                PhoneNumber = userModel.user_phone,
                                SecurityCode = RandomHelper.RandonString(5),
                                UserIdApp = user_app
                            };

                            

                        }
                        var result = await _userManager.CreateAsync(applicationUser, userModel.user_password);
                        if (result.Succeeded)
                        {
                            var confirmEmailToken = await _userManager.GenerateEmailConfirmationTokenAsync(applicationUser);

                            var encodedToken = Encoding.UTF8.GetBytes(confirmEmailToken);
                            var validToken = WebEncoders.Base64UrlEncode(encodedToken);

                            //string url = $"{_configuration["AppURL"]}/api/User/ConfirmEmail?userid={applicationUser.Id}&token={validToken}";

                            var message = new MessageServices(new string[] { applicationUser.Email }, $"Confirm your Email by Activate Code", "Welcome to Sinai" +
                                $" Please the Activate Code : '{ applicationUser.SecurityCode }'  ");
                            _emailSenderBusiness.SendEmail(message);

                            return new GeneralResponse<RegisterViewModel>
                            {
                                Message = "User created Successfully!",
                                IsSuccess = true,
                                Result = new ResultResponse<RegisterViewModel>()
                                {

                                    //activate = applicationUser.EmailConfirmed,
                                    code = applicationUser.SecurityCode,

                                    User = new RegisterViewModel
                                    {
                                        user_id = applicationUser.Id,
                                        user_name = applicationUser.FullName,
                                        user_email = applicationUser.Email,
                                        user_phone = applicationUser.PhoneNumber,
                                        user_images = applicationUser.UserImg,
                                        user_lat = locationUser.Lat,
                                        user_lng = locationUser.Lng,
                                        user_location = locationUser.Title,
                                    }
                                },
                            };
                        }
                        return new GeneralResponse<RegisterViewModel>
                        {
                            Message = "User did not create",
                            Error = new ErrorResponse
                            {
                                Details = new List<DetailsResponse>
                                {

                                    new DetailsResponse
                                    {

                                        Key = string.Join(",", result.Errors.Select(e => e.Code).ToArray()),
                                Value = string.Join(",", result.Errors.Select(e => e.Description).ToArray())
                                    }
                                }
                            }
                        };


                    }
                }
                //----------------------app id --------------------------------




                //if (userModel.user_image !=null )
                //{
                //    try
                //    {
                //        if (!Directory.Exists(_environment.WebRootPath + "\\uploads\\"))
                //        {
                //            Directory.CreateDirectory(_environment.WebRootPath + "\\uploads\\");
                //        }
                //        using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + "\\uploads\\" + RandomHelper.RandonString(25) + userModel.user_image.FileName ))
                //        {
                //            userModel.user_image.CopyTo(filestream);
                //            filestream.Flush();
                //            var userImage = "\\uploads\\" + userModel.user_image.FileName;
                //        }
                //        applicationUser = new ApplicationUser()
                //        {
                //            Email = userModel.user_email,
                //            UserName = userModel.user_email,
                //            FullName = userModel.user_name,
                //            PasswordHash = userModel.user_password,
                //            LocationId = locationUser.Id,
                //            //LocationId = 8,
                //            PhoneNumber = userModel.user_phone,
                //            UserImg = "\\uploads\\" + userModel.user_image.FileName + RandomHelper.RandonString(25),
                //            SecurityCode = RandomHelper.RandonString(5)
                //        };
                //    }
                //    catch (Exception ex)
                //    {

                //        return new GeneralResponse<RegisterViewModel>
                //        {
                //            Message = ex.Message,
                //            Error = new ErrorResponse
                //            {
                //                Details = new List<DetailsResponse>
                //            {

                //               new DetailsResponse
                //               {

                //                    Key = string.Join(",", ex.Data),
                //                    Value = string.Join(",", ex.InnerException)
                //               }
                //            }
                //            }
                //        };
                //    }
                //}
                //else
                //{
                //    applicationUser = new ApplicationUser()
                //    {
                //        Email = userModel.user_email,
                //        UserName = userModel.user_email,
                //        FullName = userModel.user_name,
                //        PasswordHash = userModel.user_password,
                //        LocationId = locationUser.Id,
                //        //LocationId = 8,
                //        PhoneNumber = userModel.user_phone,
                //        SecurityCode = RandomHelper.RandonString(5)
                //    };
                //}

                //--------------------------------------------------
                //var applicationUser = new ApplicationUser()
                //{
                //    Email = userModel.user_email,
                //    UserName = userModel.user_email,
                //    FullName = userModel.user_name,
                //    PasswordHash = userModel.user_password,
                //    LocationId = locationUser.Id,
                //    PhoneNumber = userModel.user_phone,
                //    SecurityCode = RandomHelper.RandonString(5)
                //};


                //var resultToken = await _userManager.CreateSecurityTokenAsync(applicationUser);
                //var resultToken = await _userManager.GenerateUserTokenAsync(applicationUser, "mobile", "mobile");


                //return new GeneralResponse<RegisterViewModel>
                //{
                //    Message = "User did not create",
                //    Error = new ErrorResponse
                //    {
                //        Details = new DetailsResponse
                //        {
                //            Key = string.Join(",", result.Errors.Select(e => e.Code).ToArray()),
                //            Value = string.Join(",", result.Errors.Select(e => e.Description).ToArray())
                //        },
                //    }
                //};

                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "User did not create",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = "",
                                    Value = ""
                               }
                            }
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                   Key = string.Join(",", ex.Data),
                            Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
               
            }
        }

        public async Task<GeneralResponse<RegisterViewModel>> LoginUserAsync(RegisterViewModel userModel) 
        {
            try
            {
               
                //UserManagerResponse userManager;
                var user = await _userManager.FindByEmailAsync(userModel.user_email);

                if (user == null)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "There is no user with that Email Address",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {
                                    Key = "Email",
                                    Value = "There is no user with that Email Address"
                                }
                            }
                        }
                    };

                //if(user.EmailConfirmed == false)
                //    return new GeneralResponse<RegisterViewModel>
                //    {
                //        Message = "Please Activate Email by Active code",
                //        Error = new ErrorResponse()
                //        {
                //            Details = new DetailsResponse
                //            {
                //                Key = "Activate Code",
                //                Value = "Please Activate Email by Active code"
                //            }
                //        },
                //    };

                var result = await _userManager.CheckPasswordAsync(user, userModel.user_password);

                if (!result)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Wrong password, Please Enter password again",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                                {
                                    new DetailsResponse
                                    {
                                        Key = "Password",
                                        Value = "Please Enter password again"
                                    }
                                }
                        }
                    };
                //var claims = new[]
                //{
                //    new Claim("Email", userModel.user_email),
                //    new Claim(ClaimTypes.NameIdentifier, user.Id)
                //};

                //var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));


                //var token = new JwtSecurityToken(
                //        issuer: _configuration["JWT:ValidIssuer"],
                //        audience: _configuration["JWT:ValidAudience"],
                //        expires: DateTime.Now.AddDays(86400),
                //        claims: claims,
                //        signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                //    );

                //var tokenAsString = new JwtSecurityTokenHandler().WriteToken(token);

                var symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var tokenHandler = new JwtSecurityTokenHandler();
                var now = DateTime.UtcNow;
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[]
                {
                        new Claim("Id", user.Id)
                //new Claim(ClaimTypes.Email, email)
                }),
                    NotBefore = now,
                    Expires = now.AddYears(10),
                    Issuer = _configuration["JWT:ValidIssuer"],
                    Audience = _configuration["JWT:ValidAudience"],
                    IssuedAt = now,
                    SigningCredentials = new SigningCredentials(
                symmetricKey,
                SecurityAlgorithms.HmacSha256Signature),
                };
                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(stoken);


                var location = _context.Locations.Where(loc => loc.Id == user.LocationId).SingleOrDefault();


                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "Login Successfully",
                    Result = new ResultResponse<RegisterViewModel>()
                    {

                        activate = user.EmailConfirmed,
                        access_token = token,

                        User = new RegisterViewModel
                        {
                            user_id = user.Id,
                            user_name = user.FullName,
                            user_email = user.Email,
                            user_phone = user.PhoneNumber,
                            user_images = user.UserImg,
                            user_lat = location.Lat,
                            user_lng = location.Lng,
                            user_location = location.Title,
                        }
                    },
                    IsSuccess = true
                };
            }
            catch(Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                   Key = string.Join(",", ex.Data),
                            Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }

        }

        public async Task<UserManagerResponse> ConfirmEmailAsync(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if(user == null)
                return new UserManagerResponse
                {
                    Message = "User not found",
                    IsSuccess = false,
                };

            var encodedToken = Encoding.UTF8.GetBytes(token);
            var validToken = WebEncoders.Base64UrlEncode(encodedToken);

            var result = await _userManager.ConfirmEmailAsync(user, validToken);

            if (result.Succeeded)
                return new UserManagerResponse
                {
                    Message = "Email confirm Successfully!",
                    IsSuccess = true
                };

            return new UserManagerResponse
            {
                Message = "Email did not confirm",
                IsSuccess = false,
                Details = result.Errors.Select(e => e.Description)
            };

        }

        public async Task<GeneralResponse<RegisterViewModel>> ActiveCodeAsync(string email, string activeCode)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "There is no user with that Email Address",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                                {

                                   new DetailsResponse
                                   {

                                        Key = "Email",
                                        Value = "There is no user with that Email Address"
                                   }
                                }
                        }
                    };

                //var result = await _userManager.sec
                if (user.SecurityCode == activeCode)
                {
                    user.EmailConfirmed = true;
                    var result = await _userManager.UpdateAsync(user);

                    var symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var now = DateTime.UtcNow;
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new[]
                    {
                        new Claim("Id", user.Id)
                //new Claim(ClaimTypes.Email, email)
                }),
                        NotBefore = now,
                        Expires = now.AddYears(10),
                        Issuer = _configuration["JWT:ValidIssuer"],
                        Audience = _configuration["JWT:ValidAudience"],
                        IssuedAt = now,
                        SigningCredentials = new SigningCredentials(
                    symmetricKey,
                    SecurityAlgorithms.HmacSha256Signature),
                    };
                    var stoken = tokenHandler.CreateToken(tokenDescriptor);
                    var token = tokenHandler.WriteToken(stoken);

                    var location = _context.Locations.Where(loc => loc.Id == user.LocationId).SingleOrDefault();

                    if (result.Succeeded)
                        return new GeneralResponse<RegisterViewModel>
                        {
                            Message = "Confirm Email Successfully",
                       
                            Result = new ResultResponse<RegisterViewModel>()
                            {

                                activate = user.EmailConfirmed,
                                access_token = token,

                                User = new RegisterViewModel
                                {
                                    user_id = user.Id,
                                    user_name = user.FullName,
                                    user_email = user.Email,
                                    user_phone = user.PhoneNumber,
                                    user_images = user.UserImg,
                                    user_lat = location.Lat,
                                    user_lng = location.Lng,
                                    user_location = location.Title,
                                }
                            },
                            IsSuccess = true
                        };
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "User did not create",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", result.Errors.Select(e => e.Code).ToArray()),
                            Value = string.Join(",", result.Errors.Select(e => e.Description).ToArray())
                               }
                            }
                        }
                    };
                }

                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "Activate code does not  ",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = "",
                            Value = ""
                               }
                            }
                    }
                };

            }
            catch (Exception ex)
            {
                //return new GeneralResponse<RegisterViewModel>
                //{
                //    Message = ex.Message,
                //    Error = new ErrorResponse
                //    {
                //        Details = new DetailsResponse
                //        {
                //            Key = ex.Source,
                //            Value = ex.Message
                //        }
                //    }
                //};
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.GetType())
                               }
                            }
                    }
                };
            }
            
        }
        
        public async Task<GeneralResponse<RegisterViewModel>> RestActiveCodeAsync(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "There is no user with that Email Address",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                                {

                                   new DetailsResponse
                                   {

                                        Key = "Email",
                                        Value = "There is no user with that Email Address"
                                   }
                                }
                        }
                    };


                var code = RandomHelper.RandonString(5);

                //string url = $"{_configuration["AppURL"]}/api/User/ConfirmEmail?userid={applicationUser.Id}&token={validToken}";

                var message = new MessageServices(new string[] { user.Email }, $"Reset Code", "Welcome to Sinai" +
                    $" The Active Code : '{code}'  ");
                _emailSenderBusiness.SendEmail(message);

                user.SecurityCode = code;
                var result = await _userManager.UpdateAsync(user);

                return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Send Code to Email Successfully!",
                        IsSuccess = true,
                        Result = new ResultResponse<RegisterViewModel>()
                        {
                            code = code,
                        },
                };
                
            }
            catch (Exception ex)
            {
             
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }
        }


        public async Task<GeneralResponse<RegisterViewModel>> ForgetPasswordAsync(RegisterViewModel userModel)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(userModel.user_email);

                if (user == null)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "No user associated with email",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {

                                    Key = "Email",
                                    Value = "No user associated with email"
                                }
                            }
                        }
                    };

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                var encodedToken = Encoding.UTF8.GetBytes(token);
                var validToken = WebEncoders.Base64UrlEncode(encodedToken);
                var activateCode = RandomHelper.RandonString(5);
                //string url = $"{_configuration["AppURL"]}/api/User/RestPassword?email={email}&token={validToken}";


                var message = new MessageServices(new string[] { user.Email }, "Reset Password", $"Follow the instructions to reset your password" +
                     $"To rest your password by Active Code: '{activateCode}'");
                _emailSenderBusiness.SendEmail(message);




                user.SecurityCode = activateCode;
                var result = await _userManager.UpdateAsync(user);

                if(result.Succeeded)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Reset password Code has sent to your email successfully!",
                        IsSuccess = true,
                        Result = new ResultResponse<RegisterViewModel>
                        {
                            code = activateCode
                        },
                    };
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "Activate code does not  ",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                        {

                            new DetailsResponse
                            {

                                Key = "",
                                Value = ""
                            }
                        }
                    }
                };


            }
            catch(Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }
        }

        public async Task<GeneralResponse<RegisterViewModel>> RestPasswordAsync(RegisterViewModel userModel)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(userModel.user_email);

                if (user == null)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "No user associated with email",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {

                                    Key = "Email",
                                    Value = "No user associated with email"
                                }
                            }
                        }
                    };

                if (userModel.user_password != userModel.user_password_confirm)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Confirm password doesn't match the password",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                                {

                                   new DetailsResponse
                                   {
                                         Key = "Password Or Confirm Password",
                                        Value = "Confirm password doesn't match the password"
                                   }
                                }
                        }
                    };



                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                var result = await _userManager.ResetPasswordAsync(user, token, userModel.user_password);


                //var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Password has been rest Successfully!",
                        Result = new ResultResponse<RegisterViewModel>(),
                        IsSuccess = true
                    };

            
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "Something went wrong",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", result.Errors.Select(e => e.Code).ToArray()),
                            Value = string.Join(",", result.Errors.Select(e => e.Description).ToArray())
                               }
                            }
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }
            //var user = _context.Users.Where(us => us.SecurityStamp == userModel.access_token).Select(us => new RegisterViewModel { 
            //    user_email = us.Email,
            //}).ToList();
            //var user = await _userManager.FindByNameAsync( User
            //    User.Identity.Name ??
            //    User.Claims.Where(c => c.Properties.ContainsKey("unique_name")).Select(c => c.Value).FirstOrDefault()
            //    );
            //var user = await _userManager.FindByEmailAsync(userModel.user_email);

            //if (user == null)
            //    return new UserManagerResponse
            //    {
            //        Message = "No user associated with Access Token",
            //        IsSuccess = false,
            //    };



            //var result = await _userManager.ResetPasswordAsync(user, userModel.user_password, userModel.new_user_password);

            
        }

        public async Task<GeneralResponse<RegisterViewModel>> ChangePasswordAsync(RegisterViewModel userModel, string token)
        {
            try
            {
                string userIdFromToken = GetPrincipal(token);

                var user = await _userManager.FindByIdAsync(userIdFromToken);

                //if (user == null)
                //    return new GeneralResponse<RegisterViewModel>
                //    {
                //        Message = "There is no user with that Access token",
                //        Error = new ErrorResponse
                //        {
                //            Details = new List<DetailsResponse>
                //                {
                //                    new DetailsResponse
                //                    {

                //                        Key = "Access Token",
                //                        Value = "There is no user with that Access token"
                //                    }
                //                }
                //        }
                //    };

                if (userModel.new_user_password != userModel.new_user_password_confirm)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Confirm password doesn't match the New password",
                        Error = new ErrorResponse
                        {
                            Details = new List<DetailsResponse>
                            {
                                new DetailsResponse
                                {
                                    Key = "New Password Or Confirm Password",
                                    Value = "Confirm password doesn't match the New password"
                                }
                            }
                        }
                    };

                var result = await _userManager.ChangePasswordAsync(user, userModel.user_password , userModel.new_user_password);

                if (result.Succeeded)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "User Change password Successfully!",
                        IsSuccess = true,
                        Result = new ResultResponse<RegisterViewModel>(),
                    };
   
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "User did not changed",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", result.Errors.Select(e => e.Code).ToArray()),
                            Value = string.Join(",", result.Errors.Select(e => e.Description).ToArray())
                               }
                            }
                    }
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Source),
                                    Value = string.Join(",", ex.Message)
                               }
                            }
                    }
                };
            }
        }
        public async Task<GeneralResponse<RegisterViewModel>> GetUserAsync(string token)
        {
            try
            {
                string userIdFromToken = GetPrincipal(token);

                var user = await _userManager.FindByIdAsync(userIdFromToken);

                //if (user == null)
                //    return new GeneralResponse<RegisterViewModel>
                //    {
                //        Message = "There is no user with that Access token",
                //        Error = new ErrorResponse
                //        {
                //            Details = new List<DetailsResponse>
                //                {
                //                    new DetailsResponse
                //                    {
                //                        Key = "Access Token",
                //                        Value = "There is no user with that Access token"
                //                    }
                //                }
                //        }
                //    };

                var location = _context.Locations.Where(loc => loc.Id == user.LocationId).SingleOrDefault();


                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "Information of User",
                    Result = new ResultResponse<RegisterViewModel>
                    {
                        User = new RegisterViewModel
                        {
                            user_id = user.Id,
                            user_name = user.FullName,
                            user_email = user.Email,
                            user_phone = user.PhoneNumber,
                            user_images = user.UserImg,
                            user_id_app = user.UserIdApp,
                            user_lat = location.Lat,
                            user_lng = location.Lng,
                            user_location = location.Title,
                            
                        }
                    },
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }
        }

        public async Task<GeneralResponse<RegisterViewModel>> UpdateUserAsync(RegisterViewModel userModel, string token)
        {
            try
            {
                string userIdFromToken = GetPrincipal(token);

                var user = await _userManager.FindByIdAsync(userIdFromToken);

                //if (user == null)
                //    return new GeneralResponse<RegisterViewModel>
                //    {
                //        Message = "There is no user with that Access token",
                //        Error = new ErrorResponse
                //        {
                //            Details = new List<DetailsResponse>
                //                {
                //                    new DetailsResponse
                //                    {
                //                        Key = "Access Token",
                //                        Value = "There is no user with that Access token"
                //                    }
                //                }
                //        }
                //    };

                var location = _context.Locations.Where(loc => loc.Id == user.LocationId).SingleOrDefault();

                location.Lat = userModel.user_lat;
                location.Lng = userModel.user_lng;
                location.Title = userModel.user_location;
                _context.SaveChanges();

                
                //var user_app = RandomHelper.RandonString(6);

                //var users = _context.Users.Where(us => us.UserIdApp == user_app);

                
                    
                        

                if (userModel.user_image != null)
                {
                    try
                    {
                        if (!Directory.Exists(_environment.WebRootPath + "\\uploads\\"))
                        {
                            Directory.CreateDirectory(_environment.WebRootPath + "\\uploads\\");
                        }
                        string userImage = "\\uploads\\" + RandomHelper.RandonString(25) + Path.GetExtension(userModel.user_image.FileName).ToLower();
                        using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + userImage))
                        {
                            userModel.user_image.CopyTo(filestream);
                            filestream.Flush();
                            var fileExtention = Path.GetExtension(userModel.user_image.FileName).ToLower();
                            //userImage = "\\uploads\\" + RandomHelper.RandonString(25) + fileExtention;
                        }
                        user.FullName = userModel.user_name;
                        user.Email = userModel.user_email;
                        user.PhoneNumber = userModel.user_phone;
                        user.UserImg = userImage;
                    }
                    catch (Exception ex)
                    {
                        return new GeneralResponse<RegisterViewModel>
                        {
                            Message = ex.Message,
                            Error = new ErrorResponse
                            {
                                Details = new List<DetailsResponse>
                                {

                                    new DetailsResponse
                                    {

                                        Key = string.Join(",", ex.Data),
                                        Value = string.Join(",", ex.InnerException)
                                    }
                                }
                            }
                        };
                    }
                }
                else
                {
                    user.FullName = userModel.user_name;
                    user.Email = userModel.user_email;
                    user.PhoneNumber = userModel.user_phone;

                }
                    
               


                var result = await _userManager.UpdateAsync(user);

                var locationUpdate = _context.Locations.Where(loc => loc.Id == user.LocationId).SingleOrDefault();

                if (result.Succeeded)
                    return new GeneralResponse<RegisterViewModel>
                    {
                        Message = "Information User Updated Successfully",
                        Result = new ResultResponse<RegisterViewModel>
                        {
                            User = new RegisterViewModel
                            {
                                user_id = user.Id,
                                user_email = user.Email,
                                user_name = user.FullName,
                                user_phone = user.PhoneNumber,
                                user_images = user.UserImg,
                                user_lat = locationUpdate.Lat,
                                user_lng = locationUpdate.Lng,
                                user_location = locationUpdate.Title,
                                user_id_app = user.UserIdApp,
                            }
                        },

                        IsSuccess = true
                    };


            

                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "Information User did not Updated",
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", result.Errors.Select(e => e.Code).ToArray()),
                            Value = string.Join(",", result.Errors.Select(e => e.Description).ToArray())
                               }
                            }
                    }
                };
            }
            catch(Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }
            
        }

        public async Task<GeneralResponse<RegisterViewModel>> LogOutUserAsync()
        {
            try
            {
                await _signInManager.SignOutAsync();
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = "User logged out.",
                    Result = new ResultResponse<RegisterViewModel>(),
                    IsSuccess = true,
                };
            }
            catch(Exception ex)
            {
                return new GeneralResponse<RegisterViewModel>
                {
                    Message = ex.Message,
                    Error = new ErrorResponse
                    {
                        Details = new List<DetailsResponse>
                            {

                               new DetailsResponse
                               {

                                    Key = string.Join(",", ex.Data),
                                    Value = string.Join(",", ex.InnerException)
                               }
                            }
                    }
                };
            }
        }

        public string GetPrincipal(string token)
        {
            try
            {

                var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:Secret"]));

                SecurityToken tokenValidated = null;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = signingKey,

                    ValidateAudience = true,
                    ValidAudience = _configuration["JWT:ValidAudience"],

                    ValidateIssuer = true,
                    ValidIssuer = _configuration["JWT:ValidIssuer"],

                    //ValidateLifetime = true
                    

                };
                ClaimsPrincipal principal = handler.ValidateToken(token, validationParameters, out tokenValidated);
                if (principal == null)
                    return null;
                ClaimsIdentity identity = null;
                try
                {
                    identity = (ClaimsIdentity)principal.Identity;
                }
                catch (NullReferenceException)
                {
                    return null;
                }
                Claim userIdClaim = identity.Claims.FirstOrDefault();
                string userId = userIdClaim.Value;
                return userId;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }




        }
    }
