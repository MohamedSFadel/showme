﻿using Sinai.Business_Layer.Interface;
using Sinai.Data_Access_Layer.Models;
using Sinai.Domain_Layer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Business
{
    public class TestBusiness : ITestBusiness
    {

        public List<CategoryDomainModel> getAllCat()
        {
            SinaiContext db = new SinaiContext();
            List<CategoryDomainModel> categories = db.Categories.Select(m => new CategoryDomainModel { 
                CategoryNameEn = m.CategoryNameEn
            }).ToList();
            return categories;
        }

    }
}
