﻿using Sinai.Data_Access_Layer.Models;
using Sinai.Domain_Layer.DomainModels;
using Sinai.Domain_Layer.Response;
using Sinai.Domain_Layer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Sinai.Business_Layer.Business.UserBusiness;

namespace Sinai.Business_Layer.Interface
{
    public interface IUserBusiness
    {
        Task<GeneralResponse<RegisterViewModel>> RegisterUserAsync(RegisterViewModel userModel);
        Task<GeneralResponse<RegisterViewModel>> LoginUserAsync(RegisterViewModel userModel);
        Task<UserManagerResponse> ConfirmEmailAsync(string userId, string token);
        Task<GeneralResponse<RegisterViewModel>> ActiveCodeAsync(string email, string activeCode);
        Task<GeneralResponse<RegisterViewModel>> RestActiveCodeAsync(string email);
        Task<GeneralResponse<RegisterViewModel>> ForgetPasswordAsync(RegisterViewModel userModel);
        Task<GeneralResponse<RegisterViewModel>> RestPasswordAsync(RegisterViewModel userModel);
        Task<GeneralResponse<RegisterViewModel>> ChangePasswordAsync(RegisterViewModel userModel, string token);

        Task<GeneralResponse<RegisterViewModel>> LogOutUserAsync();
        Task<GeneralResponse<RegisterViewModel>> GetUserAsync(string token);
        Task<GeneralResponse<RegisterViewModel>> UpdateUserAsync(RegisterViewModel userModel, string token);
        string GetPrincipal(string token);


        //List<CategoryDomainModel> GetAllCategoriesAsync();
    }
}
