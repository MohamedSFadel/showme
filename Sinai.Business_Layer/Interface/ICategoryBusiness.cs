﻿using Sinai.Domain_Layer.DomainModels;
using Sinai.Domain_Layer.Response;
using Sinai.Domain_Layer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Interface
{
    public interface ICategoryBusiness
    {
        GeneralResponse<CategoryDomainModel> GetCategories();
        GeneralResponse<HotelViewModel> GetHotels(int page);
        GeneralResponse<ChaletDomainModel> GetChalets(int page);
        GeneralResponse<CarDomainModel> GetCars(int page);
        GeneralResponse<TripDomainModel> GetTrips(int page);
        GeneralResponse<TourismDomainModel> GetTourisms(int page);
    }
}
