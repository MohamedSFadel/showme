﻿using Sinai.Domain_Layer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Interface
{
    public interface ITestBusiness
    {
        List<CategoryDomainModel> getAllCat();
    }
}
