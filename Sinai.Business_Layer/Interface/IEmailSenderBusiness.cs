﻿using Sinai.Domain_Layer.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Interface
{
    public interface IEmailSenderBusiness
    {
        void SendEmail(MessageServices message);
        //Task SendEmailAsync(MessageServices mailRequest);
    }
}
