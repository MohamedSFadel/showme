﻿using Sinai.Business_Layer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Repository
{
    public interface IUnitOfWorkTest : IDisposable
    {
        ICategoryBusiness Categories { get; }
        //IProjectRepository Projects { get; }
        int Complete();
    }
}
