﻿using Microsoft.EntityFrameworkCore;
using Sinai.Data_Access_Layer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly SinaiContext _sinaiContext;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(SinaiContext sinaiContext)
        {
            _sinaiContext = sinaiContext;

            if (sinaiContext != null)
            {
                _dbSet = sinaiContext.Set<TEntity>();
            }
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync().ConfigureAwait(false);
        }
    }
}
