﻿using Sinai.Business_Layer.Business;
using Sinai.Business_Layer.Interface;
using Sinai.Data_Access_Layer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Business_Layer.Repository
{
    public class UnitOfWorkTest : IUnitOfWorkTest
    {
        private readonly SinaiContext _context;
        public ICategoryBusiness Categories { get; private set; }

        public UnitOfWorkTest(SinaiContext context)
        {
            _context = context;
            //Categories = new CategoryBusiness(_context);
            //Projects = new ProjectRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
