﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class ReservationHotelChalet
    {
        public ReservationHotelChalet()
        {
            ReservationRoomIds = new HashSet<ReservationRoomId>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime? CheckInDay { get; set; }
        public DateTime? CheckOutDay { get; set; }
        public int MealId { get; set; }

        public virtual Meal Meal { get; set; }
        public virtual ICollection<ReservationRoomId> ReservationRoomIds { get; set; }
    }
}
