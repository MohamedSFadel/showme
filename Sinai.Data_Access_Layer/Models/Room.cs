﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Room
    {
        public Room()
        {
            ReservationRoomIds = new HashSet<ReservationRoomId>();
        }

        public int Id { get; set; }
        public string RoomNum { get; set; }
        public string Details { get; set; }
        public double? RoomSize { get; set; }
        public int? MaxPerson { get; set; }
        public int? MaxKids { get; set; }
        public string RoomImg { get; set; }
        public double? RoomPrice { get; set; }
        public int? Rate { get; set; }
        public int HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }
        public virtual ICollection<ReservationRoomId> ReservationRoomIds { get; set; }
    }
}
