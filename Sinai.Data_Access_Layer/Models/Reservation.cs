﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Reservation
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }
        public int ReservationCategoryId { get; set; }
        public int PaymentId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Payment Payment { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
