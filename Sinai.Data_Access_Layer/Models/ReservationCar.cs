﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class ReservationCar
    {
        public int Id { get; set; }
        public bool? PerDay { get; set; }
        public DateTime? StartDay { get; set; }
        public DateTime? EndDay { get; set; }
        public bool? PerHour { get; set; }
        public TimeSpan? StartHour { get; set; }
        public TimeSpan? EndHour { get; set; }
        public bool? Driver { get; set; }
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
    }
}
