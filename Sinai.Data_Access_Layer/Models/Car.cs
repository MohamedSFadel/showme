﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Car
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string Phone { get; set; }
        public double? CarPrice { get; set; }
        public bool? Driver { get; set; }
        public int CategoryId { get; set; }
        public bool? Active { get; set; }

        public virtual Category Category { get; set; }
    }
}
