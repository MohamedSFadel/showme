﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Category
    {
        public Category()
        {
            Cars = new HashSet<Car>();
            Chalets = new HashSet<Chalet>();
            Faclities = new HashSet<Faclity>();
            Hotels = new HashSet<Hotel>();
            Meals = new HashSet<Meal>();
            Media = new HashSet<Media>();
            NearByPlaces = new HashSet<NearByPlace>();
            Offers = new HashSet<Offer>();
            Policities = new HashSet<Policity>();
            Reservations = new HashSet<Reservation>();
            Reviews = new HashSet<Review>();
            Tourisms = new HashSet<Tourism>();
            Trips = new HashSet<Trip>();
        }

        public int Id { get; set; }
        public string CategoryNameAr { get; set; }
        public string CategoryNameEn { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<Car> Cars { get; set; }
        public virtual ICollection<Chalet> Chalets { get; set; }
        public virtual ICollection<Faclity> Faclities { get; set; }
        public virtual ICollection<Hotel> Hotels { get; set; }
        public virtual ICollection<Meal> Meals { get; set; }
        public virtual ICollection<Media> Media { get; set; }
        public virtual ICollection<NearByPlace> NearByPlaces { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
        public virtual ICollection<Policity> Policities { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Tourism> Tourisms { get; set; }
        public virtual ICollection<Trip> Trips { get; set; }
    }
}
