﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public class City
    {
        public City()
        {
            Locations = new HashSet<Location>();
        }
        [Key]
        public int Id { get; set; }
        public string CityNameAr { get; set; }
        public string CityNameEn { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }
}
