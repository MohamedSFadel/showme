﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Review
    {
        public int Id { get; set; }
        public string Details { get; set; }
        public DateTime? Date { get; set; }
        public int? Rate { get; set; }
        public string UserId { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }
        public int? RoomId { get; set; }

        public virtual Category Category { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
