﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class ReservationRoomId
    {
        public int Id { get; set; }
        public int ReservationHotelId { get; set; }
        public int RoomId { get; set; }

        public virtual ReservationHotelChalet ReservationHotel { get; set; }
        public virtual Room Room { get; set; }
    }
}
