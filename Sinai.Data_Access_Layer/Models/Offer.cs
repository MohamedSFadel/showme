﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Offer
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public double? Discount { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }
        public int? RoomId { get; set; }

        public virtual Category Category { get; set; }
    }
}
