﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Tourism
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public int CategoryId { get; set; }
        public int LocationId { get; set; }
        public bool? Active { get; set; }


        public virtual Category Category { get; set; }
        public virtual Location Location { get; set; }
    }
}
