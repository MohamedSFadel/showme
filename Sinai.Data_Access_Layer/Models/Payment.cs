﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Payment
    {
        public Payment()
        {
            Reservations = new HashSet<Reservation>();
        }

        public int Id { get; set; }
        public double? Price { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}
