﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class Location
    {
        public Location()
        {
            Chalets = new HashSet<Chalet>();
            Hotels = new HashSet<Hotel>();
            NearByPlaces = new HashSet<NearByPlace>();
            ReservationCars = new HashSet<ReservationCar>();
            Tourisms = new HashSet<Tourism>();
            Trips = new HashSet<Trip>();
            Users = new HashSet<ApplicationUser>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        //public int CityId { get; set; }

        public virtual ICollection<Chalet> Chalets { get; set; }
        public virtual ICollection<Hotel> Hotels { get; set; }
        public virtual ICollection<NearByPlace> NearByPlaces { get; set; }
        public virtual ICollection<ReservationCar> ReservationCars { get; set; }
        public virtual ICollection<Tourism> Tourisms { get; set; }
        public virtual ICollection<Trip> Trips { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
       // public virtual City City { get; set; }



    }
}
