﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Models
{
    public partial class SinaiContext : IdentityDbContext<ApplicationUser>
    {
        public SinaiContext()
        {
        }

        public SinaiContext(DbContextOptions<SinaiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Chalet> Chalets { get; set; }
        public virtual DbSet<Faclity> Faclities { get; set; }
        public virtual DbSet<Hotel> Hotels { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        //public virtual DbSet<Login> Logins { get; set; }
        public virtual DbSet<Meal> Meals { get; set; }
        public virtual DbSet<Media> Media { get; set; }
        public virtual DbSet<NearByPlace> NearByPlaces { get; set; }
        public virtual DbSet<Offer> Offers { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Policity> Policities { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }
        public virtual DbSet<ReservationCar> ReservationCars { get; set; }
        public virtual DbSet<ReservationHotelChalet> ReservationHotelChalets { get; set; }
        public virtual DbSet<ReservationRoomId> ReservationRoomIds { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
       // public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Tourism> Tourisms { get; set; }
        public virtual DbSet<Trip> Trips { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-1EO6KQ3;Database=Sinai;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Arabic_CI_AS");

            modelBuilder.Entity<Car>(entity =>
            {
                entity.ToTable("Car");

                entity.Property(e => e.CarPrice).HasColumnName("Car_price");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.Property(e => e.CategoryNameAr)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Category_name_ar");

                entity.Property(e => e.CategoryNameEn)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Category_name_en");
            });
            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("City");

                entity.Property(e => e.CityNameAr)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("City_name_ar");

                entity.Property(e => e.CityNameEn)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("City_name_en");
            });

            modelBuilder.Entity<Chalet>(entity =>
            {
                entity.ToTable("Chalet");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.ChaletPrice).HasColumnName("Chalet_price");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId).HasColumnName("Location_id");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoomCount).HasColumnName("Room_count");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Chalets)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Chalets)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Faclity>(entity =>
            {
                entity.ToTable("Faclity");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.Key)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Faclities)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Hotel>(entity =>
            {
                entity.ToTable("Hotel");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId).HasColumnName("Location_id");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Hotels)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Hotels)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("Location");
                //entity.Property(e => e.CityId).HasColumnName("City_id");

                entity.Property(e => e.Lat)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lng)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.HasOne(d => d.City)
                //   .WithMany(p => p.Locations)
                //   .HasForeignKey(d => d.CityId)
                //   .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Login>(entity =>
            {
                entity.ToTable("Login");

                entity.Property(e => e.AccessToken)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Access_token");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("Role_id");

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Logins)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                
            });

            modelBuilder.Entity<Meal>(entity =>
            {
                entity.ToTable("Meal");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Meals)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Media>(entity =>
            {
                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.MediaType)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Media_type");

                entity.Property(e => e.MediaUrl)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Media_url");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Media)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            

            modelBuilder.Entity<NearByPlace>(entity =>
            {
                entity.ToTable("Near_By_Place");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.LocationId).HasColumnName("Location_id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.NearByPlaces)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.NearByPlaces)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.ToTable("Offer");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RoomId).HasColumnName("Room_id");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Offers)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.ToTable("Payment");
            });

            modelBuilder.Entity<Policity>(entity =>
            {
                entity.ToTable("Policity");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.Key)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Policities)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.ToTable("Reservation");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.PaymentId).HasColumnName("Payment_id");

                entity.Property(e => e.ReservationCategoryId).HasColumnName("Reservation_category_id");

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Reservations)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Payment)
                    .WithMany(p => p.Reservations)
                    .HasForeignKey(d => d.PaymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Reservations)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ReservationCar>(entity =>
            {
                entity.ToTable("Reservation_Car");

                entity.Property(e => e.EndDay)
                    .HasColumnType("date")
                    .HasColumnName("End_day");

                entity.Property(e => e.EndHour).HasColumnName("End_hour");

                entity.Property(e => e.LocationId).HasColumnName("Location_id");

                entity.Property(e => e.PerDay).HasColumnName("Per_day");

                entity.Property(e => e.PerHour).HasColumnName("Per_hour");

                entity.Property(e => e.StartDay)
                    .HasColumnType("date")
                    .HasColumnName("Start_day");

                entity.Property(e => e.StartHour).HasColumnName("Start_hour");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.ReservationCars)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ReservationHotelChalet>(entity =>
            {
                entity.ToTable("Reservation_Hotel_Chalet");

                entity.Property(e => e.CheckInDay)
                    .HasColumnType("date")
                    .HasColumnName("Check_in_day");

                entity.Property(e => e.CheckOutDay)
                    .HasColumnType("date")
                    .HasColumnName("Check_out_day");

                entity.Property(e => e.MealId).HasColumnName("Meal_id");

                entity.Property(e => e.Type)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Meal)
                    .WithMany(p => p.ReservationHotelChalets)
                    .HasForeignKey(d => d.MealId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ReservationRoomId>(entity =>
            {
                entity.ToTable("Reservation_Room_Id");

                entity.Property(e => e.ReservationHotelId).HasColumnName("Reservation_hotel_id");

                entity.Property(e => e.RoomId).HasColumnName("Room_id");

                entity.HasOne(d => d.ReservationHotel)
                    .WithMany(p => p.ReservationRoomIds)
                    .HasForeignKey(d => d.ReservationHotelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.ReservationRoomIds)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Review>(entity =>
            {
                entity.ToTable("Review");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.CategoryTypeId).HasColumnName("Category_type_id");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Rate).HasColumnName("rate");

                entity.Property(e => e.RoomId).HasColumnName("Room_id");

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Reviews)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Reviews)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.RoleDesc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Role_desc");

                entity.Property(e => e.RoleNameAr)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Role_name_ar");

                entity.Property(e => e.RoleNameEn)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Role_name_en");
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.ToTable("Room");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.HotelId).HasColumnName("Hotel_id");

                entity.Property(e => e.MaxKids).HasColumnName("Max_kids");

                entity.Property(e => e.MaxPerson).HasColumnName("Max_person");

                entity.Property(e => e.Rate).HasColumnName("rate");

                entity.Property(e => e.RoomImg)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Room_img");

                entity.Property(e => e.RoomNum)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Room_num");

                entity.Property(e => e.RoomPrice).HasColumnName("Room_price");

                entity.Property(e => e.RoomSize).HasColumnName("Room_size");

                entity.HasOne(d => d.Hotel)
                    .WithMany(p => p.Rooms)
                    .HasForeignKey(d => d.HotelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Tourism>(entity =>
            {
                entity.ToTable("Tourism");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId).HasColumnName("Location_id");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Tourisms)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Tourisms)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Trip>(entity =>
            {
                entity.ToTable("Trip");

                entity.Property(e => e.CategoryId).HasColumnName("Category_id");

                entity.Property(e => e.Details)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EndDay)
                    .HasColumnType("date")
                    .HasColumnName("End_day");

                entity.Property(e => e.LocationId).HasColumnName("Location_id");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDay)
                    .HasColumnType("date")
                    .HasColumnName("Start_day");

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TripPrice).HasColumnName("Trip_price");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Trips)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Trips)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>()
                                           .HasIndex(b => b.PhoneNumber)
                                           .IsUnique();


            modelBuilder.Entity<ApplicationUser>().ToTable("User");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole<string>>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim<string>>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin<string>>().ToTable("UserLogin");
            modelBuilder.Entity<IdentityRoleClaim<string>>().ToTable("RoleClaim");
            modelBuilder.Entity<IdentityUserToken<string>>().ToTable("UserToken");


            OnModelCreatingPartial(modelBuilder);
            
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
