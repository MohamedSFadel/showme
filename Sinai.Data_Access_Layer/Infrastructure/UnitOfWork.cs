﻿using Microsoft.EntityFrameworkCore;
using Sinai.Data_Access_Layer.Infrastructure.Contract;
using Sinai.Data_Access_Layer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Data_Access_Layer.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
     
        private readonly SinaiContext _dbContext;

        public UnitOfWork()
        {
            _dbContext = new SinaiContext();
        }

        public DbContext Db
        {
            get { return _dbContext; }
        }

        public void Dispose()
        {
        }
    }
}
