﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sinai.Data_Access_Layer.Migrations
{
    public partial class SinaiDatabase4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_City_CityId",
                table: "Location");

            migrationBuilder.RenameColumn(
                name: "CityId",
                table: "Location",
                newName: "City_id");

            migrationBuilder.RenameIndex(
                name: "IX_Location_CityId",
                table: "Location",
                newName: "IX_Location_City_id");

            migrationBuilder.RenameColumn(
                name: "CityNameEn",
                table: "City",
                newName: "City_name_en");

            migrationBuilder.RenameColumn(
                name: "CityNameAr",
                table: "City",
                newName: "City_name_ar");

            migrationBuilder.AlterColumn<string>(
                name: "City_name_en",
                table: "City",
                type: "varchar(255)",
                unicode: false,
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "City_name_ar",
                table: "City",
                type: "varchar(255)",
                unicode: false,
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_City_City_id",
                table: "Location",
                column: "City_id",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_City_City_id",
                table: "Location");

            migrationBuilder.RenameColumn(
                name: "City_id",
                table: "Location",
                newName: "CityId");

            migrationBuilder.RenameIndex(
                name: "IX_Location_City_id",
                table: "Location",
                newName: "IX_Location_CityId");

            migrationBuilder.RenameColumn(
                name: "City_name_en",
                table: "City",
                newName: "CityNameEn");

            migrationBuilder.RenameColumn(
                name: "City_name_ar",
                table: "City",
                newName: "CityNameAr");

            migrationBuilder.AlterColumn<string>(
                name: "CityNameEn",
                table: "City",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldUnicode: false,
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CityNameAr",
                table: "City",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldUnicode: false,
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_City_CityId",
                table: "Location",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
