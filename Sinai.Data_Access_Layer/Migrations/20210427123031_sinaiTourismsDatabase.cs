﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sinai.Data_Access_Layer.Migrations
{
    public partial class sinaiTourismsDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Tourism",
                type: "bit",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Tourism");
        }
    }
}
