﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sinai.Data_Access_Layer.Migrations
{
    public partial class SinaiNotCityDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_City_City_id",
                table: "Location");

            migrationBuilder.RenameColumn(
                name: "City_id",
                table: "Location",
                newName: "CityId");

            migrationBuilder.RenameIndex(
                name: "IX_Location_City_id",
                table: "Location",
                newName: "IX_Location_CityId");

            migrationBuilder.AlterColumn<int>(
                name: "CityId",
                table: "Location",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_City_CityId",
                table: "Location",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_City_CityId",
                table: "Location");

            migrationBuilder.RenameColumn(
                name: "CityId",
                table: "Location",
                newName: "City_id");

            migrationBuilder.RenameIndex(
                name: "IX_Location_CityId",
                table: "Location",
                newName: "IX_Location_City_id");

            migrationBuilder.AlterColumn<int>(
                name: "City_id",
                table: "Location",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_City_City_id",
                table: "Location",
                column: "City_id",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
