﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class MediaDomainModel
    {
        public int Id { get; set; }
        public string MediaType { get; set; }
        public string MediaUrl { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }

        public virtual CategoryDomainModel Category { get; set; }
    }
}
