﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class RoomDomainModel
    {
        public RoomDomainModel()
        {
            ReservationRoomIds = new HashSet<ReservationRoomIdDomainModel>();
        }

        public int Id { get; set; }
        public string RoomNum { get; set; }
        public string Details { get; set; }
        public double? RoomSize { get; set; }
        public int? MaxPerson { get; set; }
        public int? MaxKids { get; set; }
        public string RoomImg { get; set; }
        public double? RoomPrice { get; set; }
        public int? Rate { get; set; }
        public int HotelId { get; set; }
        public float? rate { get; set; }

        public virtual HotelDomainModel Hotel { get; set; }
        public virtual ICollection<ReservationRoomIdDomainModel> ReservationRoomIds { get; set; }
    }
}
