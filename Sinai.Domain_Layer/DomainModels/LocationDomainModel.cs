﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class LocationDomainModel
    {
        public LocationDomainModel()
        {
            Chalets = new HashSet<ChaletDomainModel>();
            Hotels = new HashSet<HotelDomainModel>();
            NearByPlaces = new HashSet<NearByPlaceDomainModel>();
            ReservationCars = new HashSet<ReservationCarDomainModel>();
            Tourisms = new HashSet<TourismDomainModel>();
            Trips = new HashSet<TripDomainModel>();
           // Users = new HashSet<ApplicationUser>();
           // Users = new HashSet<ApplicationUser>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public int CityId { get; set; }

        public virtual ICollection<ChaletDomainModel> Chalets { get; set; }
        public virtual ICollection<HotelDomainModel> Hotels { get; set; }
        public virtual ICollection<NearByPlaceDomainModel> NearByPlaces { get; set; }
        public virtual ICollection<ReservationCarDomainModel> ReservationCars { get; set; }
        public virtual ICollection<TourismDomainModel> Tourisms { get; set; }
        public virtual ICollection<TripDomainModel> Trips { get; set; }
        //public virtual ICollection<ApplicationUser> Users { get; set; }
        //public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}
