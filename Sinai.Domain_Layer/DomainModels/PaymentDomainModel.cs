﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class PaymentDomainModel
    {
        public PaymentDomainModel()
        {
            Reservations = new HashSet<ReservationDomainModel>();
        }

        public int Id { get; set; }
        public double? Price { get; set; }

        public virtual ICollection<ReservationDomainModel> Reservations { get; set; }
    }
}
