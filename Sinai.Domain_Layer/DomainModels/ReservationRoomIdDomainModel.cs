﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class ReservationRoomIdDomainModel
    {
        public int Id { get; set; }
        public int ReservationHotelId { get; set; }
        public int RoomId { get; set; }

        public virtual ReservationHotelChaletDomainModel ReservationHotel { get; set; }
        public virtual RoomDomainModel Room { get; set; }
    }
}
