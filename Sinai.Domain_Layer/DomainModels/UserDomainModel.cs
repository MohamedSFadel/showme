﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public class UserDomainModel
    {
        //[Required]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(50, MinimumLength =5)]
        public string Password { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string ConfirmPassword { get; set; }

        //[Required]
        //[StringLength(50)]
        public string UserName { get; set; }

        //[Required]
        public string PhoneNumber { get; set; }
        public int LoctionId { get; set; }
        public string SecurityCode { get; set; }
        public string UserImg { get; set; }
        

    }
}
