﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class NearByPlaceDomainModel
    {
        public int Id { get; set; }
        public double? Destance { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }
        public int LocationId { get; set; }

        public virtual CategoryDomainModel Category { get; set; }
        public virtual LocationDomainModel Location { get; set; }
    }
}
