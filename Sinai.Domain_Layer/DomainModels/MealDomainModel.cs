﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class MealDomainModel
    {
        public MealDomainModel()
        {
            ReservationHotelChalets = new HashSet<ReservationHotelChaletDomainModel>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }

        public virtual CategoryDomainModel Category { get; set; }
        public virtual ICollection<ReservationHotelChaletDomainModel> ReservationHotelChalets { get; set; }
    }
}
