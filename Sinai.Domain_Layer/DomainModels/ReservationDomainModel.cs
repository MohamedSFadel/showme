﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class ReservationDomainModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int CategoryId { get; set; }
        public int CategoryTypeId { get; set; }
        public int ReservationCategoryId { get; set; }
        public int PaymentId { get; set; }

        public virtual CategoryDomainModel Category { get; set; }
        public virtual PaymentDomainModel Payment { get; set; }
        //public virtual ApplicationUser User { get; set; }
    }
}
