﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class HotelDomainModel
    {
        //public HotelDomainModel()
        //{
        //    Rooms = new HashSet<RoomDomainModel>();
        //}

        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public int CategoryId { get; set; }
        public int LocationId { get; set; }
        public bool? Active { get; set; }
        
        //public virtual CategoryDomainModel Category { get; set; }
        public virtual LocationDomainModel Location { get; set; }
        //public virtual ICollection<RoomDomainModel> Rooms { get; set; }
    }
}
