﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public class CategoryDomainModel
    {
        //public CategoryDomainModel()
        //{
        //    Cars = new HashSet<CarDomainModel>();
        //    Chalets = new HashSet<ChaletDomainModel>();
        //    Faclities = new HashSet<FaclityDomainModel>();
        //    Hotels = new HashSet<HotelDomainModel>();
        //    Meals = new HashSet<MealDomainModel>();
        //    Media = new HashSet<MediaDomainModel>();
        //    NearByPlaces = new HashSet<NearByPlaceDomainModel>();
        //    Offers = new HashSet<OfferDomainModel>();
        //    Policities = new HashSet<PolicityDomainModel>();
        //    Reservations = new HashSet<ReservationDomainModel>();
        //    Reviews = new HashSet<ReviewDomainModel>();
        //    Tourisms = new HashSet<TourismDomainModel>();
        //    Trips = new HashSet<TripDomainModel>();
        //}

        public int Id { get; set; }
        public string CategoryNameAr { get; set; }
        public string CategoryNameEn { get; set; }
        public bool? Active { get; set; }

        //public virtual ICollection<CarDomainModel> Cars { get; set; }
        //public virtual ICollection<ChaletDomainModel> Chalets { get; set; }
        //public virtual ICollection<FaclityDomainModel> Faclities { get; set; }
        //public virtual ICollection<HotelDomainModel> Hotels { get; set; }
        //public virtual ICollection<MealDomainModel> Meals { get; set; }
        //public virtual ICollection<MediaDomainModel> Media { get; set; }
        //public virtual ICollection<NearByPlaceDomainModel> NearByPlaces { get; set; }
        //public virtual ICollection<OfferDomainModel> Offers { get; set; }
        //public virtual ICollection<PolicityDomainModel> Policities { get; set; }
        //public virtual ICollection<ReservationDomainModel> Reservations { get; set; }
        //public virtual ICollection<ReviewDomainModel> Reviews { get; set; }
        //public virtual ICollection<TourismDomainModel> Tourisms { get; set; }
        //public virtual ICollection<TripDomainModel> Trips { get; set; }
    }
}
