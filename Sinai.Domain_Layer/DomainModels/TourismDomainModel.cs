﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class TourismDomainModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string details { get; set; }
        public int category_id { get; set; }
        //public int LocationId { get; set; }

        public bool? active { get; set; }
        public float? rate { get; set; }

        public string location_name { get; set; }
        public double location_lat { get; set; }
        public double location_lng { get; set; }
        public int city_id { get; set; }


        public string image { get; set; }
        public List<MediaDomainModel> images { get; set; }


        //public virtual CategoryDomainModel Category { get; set; }
        //public virtual LocationDomainModel Location { get; set; }
    }
}
