﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.DomainModels
{
    public partial class ReservationHotelChaletDomainModel
    {
        public ReservationHotelChaletDomainModel()
        {
            ReservationRoomIds = new HashSet<ReservationRoomIdDomainModel>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime? CheckInDay { get; set; }
        public DateTime? CheckOutDay { get; set; }
        public int MealId { get; set; }

        public virtual MealDomainModel Meal { get; set; }
        public virtual ICollection<ReservationRoomIdDomainModel> ReservationRoomIds { get; set; }
    }
}
