﻿using Sinai.Domain_Layer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.Response
{
    public class GeneralResponse<TEntity> where TEntity : class
    {
        public string Message { get; set; }
        public ErrorResponse Error { get; set; }
        //public TEntity Result { get; set; }
        public ResultResponse<TEntity> Result { get; set; } 

        [JsonIgnore]
        public bool IsSuccess { get; set; }


    }

    public class ResultResponse<TEntity> where TEntity : class
    {
        public string code { get; set; }
        public string access_token { get; set; }

        public bool activate { get; set; }
        public TEntity User { get; set; }
        public List<TEntity> Categories { get; set; }
        public IEnumerable<TEntity> Hotels { get; set; }
        public IEnumerable<TEntity> Chalets { get; set; }
        public IEnumerable<TEntity> Cars { get; set; }
        public IEnumerable<TEntity> Trips { get; set; }
        public IEnumerable<TEntity> Tourisms { get; set; }

    }
    

}
