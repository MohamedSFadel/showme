﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.Response
{
    public class ErrorResponse
    {
        public List<DetailsResponse> Details { get; set; }

    }
    public class DetailsResponse
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
