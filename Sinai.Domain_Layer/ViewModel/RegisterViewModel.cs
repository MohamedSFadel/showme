﻿using Microsoft.AspNetCore.Http;
using Sinai.Domain_Layer.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.ViewModel
{
    public class RegisterViewModel
    {
        //public UserManagerResponse message { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string user_email { get; set; }
        public string user_password { get; set; }
        public string new_user_password { get; set; }
        public string user_password_confirm { get; set; }
        public string new_user_password_confirm { get; set; }
        public string user_phone { get; set; }
        public IFormFile user_image { get; set; }
        public string user_images { get; set; }
        public string user_id_app { get; set; }
        public string access_token { get; set; }
        
        public string code { get; set; }
        
        public double user_lat { get; set; } 
        public double user_lng { get; set; }
        public string user_location { get; set; }
    }


}
