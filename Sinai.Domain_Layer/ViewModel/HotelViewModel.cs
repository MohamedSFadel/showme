﻿using Sinai.Domain_Layer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.ViewModel
{
    public class HotelViewModel
    {

        public int id { get; set; }
        public string title { get; set; }
        public string details { get; set; }
        public double start_price { get; set; }
        public int CategoryId { get; set; }
        public int LocationId { get; set; }
        public bool? Active { get; set; }
        public float? rate { get; set; }

        public string Location_name { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int CityId { get; set; }
        
        //public string hotel_image_type { get; set; }
        //public string hotel_image { get; set; }
        //public int CategoryTypeId { get; set; }


        public string image { get; set; }
        public List<MediaDomainModel> images { get; set; }
    }
}
