﻿using Sinai.Domain_Layer.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sinai.Domain_Layer.ViewModel
{
    public class GenericCategoryTypeViewModel
    {
        public HotelViewModel CategoryType { get; set; }
        public List<MediaDomainModel> Media { get; set; }

    }
}
